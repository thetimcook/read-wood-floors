$(document).ready(function(){

	$('nav > ul > li').each(function(){
		var liPosition = $(this).position().left;
		$(this).css('background-position-x', -liPosition);
		$(this).find('.dropdown').css('background-position-x', -liPosition);
		var thisWidth = $(this).css('width');
		$(this).find('.dropdown').css('min-width', thisWidth);
	});
	
	var matchHeight = function() {
		var heightOne = $('.grid620').outerHeight();
		var heightTwo = $('.grid300').outerHeight();
		console.log(heightOne);
		if (heightOne > heightTwo) {
			$('.grid300').css('height', heightOne +'px');
		} else if (heightOne < heightTwo) {
			$('.grid620').css('height', heightTwo +'px');
		}
	}
/* 	matchHeight(); */
/* 	$('.subbox').equalHeightColumns(); */
	

	var toggleOn = 0
	var navOffset = $('nav').offset().top;
	var stickyNav = function() {
		var scrollPos = $(window).scrollTop();
		var iAm = $('#iam').css('opacity');
		if (scrollPos > navOffset) {
			$('nav').css({ 'position': 'fixed', 'top': 0 });
			$('nav').addClass('fixedNav').removeClass('normalNav');
			if (toggleOn === 0) {
				$('#iam').animate({opacity: 0}, 300, function(){
					$('.mini-logo').animate({top: '0px'}, 300);
				});
				toggleOn = 1;
			} else {
				
			}
			console.log(toggleOn);
/* 			$('nav a').animate({color: 'white' }, 300); */
		} else {
			$('nav').css({ 'position': 'relative' });
			$('nav').addClass('normalNav').removeClass('fixedNav');
			if (toggleOn === 1) {
				$('.mini-logo').animate({top: '-85px'}, 300, function(){
					$('#iam').animate({opacity: 1}, 300);
				});
				toggleOn = 0;
			} else {
				
			}
			console.log(toggleOn);
/* 			$('nav a').animate({color: '#333' }, 300); */
		}
	};
	
	stickyNav();
	$(window).scroll(function(){
		stickyNav();
	})

	$('#cta-slider').flexslider({
		animation: 'slide'
	});
	
	$('#product-slider').flexslider({
		animation: 'slide'
	});
	
	$('#quotes').flexslider({
		animation: 'fade',
		directionNav: false,
		controlNav: false,
		slideshowSpeed: 5000,
		animationSpeed: 1000,
		randomize: true
	});

	
/*
	$('#cta-slider').flexslider({
		animation: 'slide',
		start: function(){
			var selectedP = $('.flex-active-slide').attr('id');
			$('.flex-active-slide').children('.cta-box-container').blurjs({
				source: '#' + selectedP,
				overlay: 'rgba(255,255,255,0.6)',
				radius: 10
			});
		},
		after: function(){
			var selectedP = $('.flex-active-slide').attr('id');
			$('.flex-active-slide').children('.cta-box-container').blurjs({
				source: '#' + selectedP,
				overlay: 'rgba(255,255,255,0.6)',
				radius: 10
			});
		}
	});
*/

	$('p').dotdotdot();

	$('.blur-img').each(function(){
		$(this).blurjs({
			source: '.blur-img',
			radius: 13
		});
	});
	var importImages = function(){
		for (var i=0; i < 7; i++) {
			$('.layers-img').append('<img class="finish-layers" src="images/finish-layer-2.png"/>');
			$('.layers-img').find(':last-child').animate({opacity: 1, 'top': '-'+ (i+1)*10 +'px'}, 200);
		};
	};
	$('.layers-img').hover(function(){
		var imgChild = $('.layers-img').find(':last-child');
		if (imgChild.hasClass('finish-layers') === false) {
			$('.layers-img img.layer1').animate({opacity: 1}, 300, function() {
				importImages();
			});
		} else {
			$('.finish-layers').remove();
		}
		
	}, function(){
		$('.finish-layers').clearQueue();
		$('.finish-layers').animate({opacity: 0, 'top': '0px'}, 300, function(){
			$('.finish-layers').remove();
		});
		$('.layers-img img.layer1').animate({opacity: 0}, 500);
	});
	
/*
	$(window).resize(function(){
		var hPosition = $('#header').css('margin-left');
		$('li').hover(function(){
			var liPosition = $(this).position().left;
			$(this).css('background-position-x', -liPosition);
		});

	});
*/
	$('.box').mouseenter(function(){
		$(this).stop().animate({
			'boxShadow': 'inset 0px 0px 0px 0px rgba(255,255,255,1)',
			'z-index': '10'
		}, 200);
		$(".label", this).stop().animate({
			'top': '150px'
		}, 200, function(){
			$(".specs", this).animate({
				'opacity': '1'
			}, 200);
		});
		
	}).mouseleave(function(){
		$(this).stop().animate({
			'boxShadow': 'inset 0px 0px 0px 20px rgba(255,255,255,1)',
			'z-index': '1'
		}, 200);
		$(".specs", this).stop().animate({
			'opacity': '0'
		}, 100);
		$(".label", this).stop().animate({
			'top': '240px'
		}, 200);
	});

	$('.box').click(function(){
		if ($(this).css('width') === '255px') {
				$(this).stop().animate({
					'width': '510px',
					'height': '540px',
					'boxShadow': 'inset 0px 0px 0px 0px rgba(255,255,255,1)',
					'z-index': '10'
				}, 200).css('border', '1px solid #aaa').css('cursor', 'default');
			$(this).mouseleave(function(){
				$(this).stop().animate({
					'boxShadow': 'inset 0px 0px 0px 20px rgba(255,255,255,1)',
					'width': '255px',
					'height': '300px',
					'background-position-x': '0',
					'z-index': '1'
				}, 200).css('border', '0px solid #aaa').css('cursor', 'pointer');
				$(this).children().find('a.select').removeClass('select');
				$(this).children().find('a:first-child').addClass('select');
			});
		} else {
			
		};
	});
	$('.view-1').click(function(){
		$(this).parent().children('a.select').removeClass('select');
		$(this).addClass('select');
		var x = $(this).parent('div');
		x.parent('div').animate({ backgroundPositionX: "0px" }, 500);
		
	});
	$('.view-2').click(function(){
		$(this).parent().children('a.select').removeClass('select');
		$(this).addClass('select');
		var x = $(this).parent('div');
		x.parent('div').animate({ backgroundPositionX: "-640px" }, 500);
	});
	$('.view-3').click(function(){
		$(this).parent().children('a.select').removeClass('select');
		$(this).addClass('select');
		var x = $(this).parent('div');
		x.parent('div').animate({ backgroundPositionX: "-1280px" }, 500);
	});
	
	
	
});